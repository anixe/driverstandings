import { Component, OnInit, Input } from '@angular/core';
import { Details } from '../../../../models/details';

@Component({
  selector: 'ds-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  @Input() details: Details[];
  @Input() url: string;
  @Input() isDriver: boolean;
  public linkLabel: string;

  constructor() { }

  ngOnInit() {
    if(this.isDriver){
      this.linkLabel = 'Biography';
    }else{
      this.linkLabel = 'Info';
    }
  }

  openInNewTab(url: string){
    window.open(url, "_blank");
  }
}
