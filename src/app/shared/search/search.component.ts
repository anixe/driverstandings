import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormControl} from '@angular/forms';
import { debounce, debounceTime, subscribeOn } from '../../../../node_modules/rxjs/operators';

@Component({
  selector: 'ds-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() searchClicked = new EventEmitter<string>();
  private readonly delay = 1000;
  public searchCriteriaControl = new FormControl();

  constructor() { }

  ngOnInit() {
    this.searchCriteriaControl.valueChanges.pipe(
      debounceTime(this.delay)
    ).subscribe((value) => {
      this.searchClicked.emit(value);
    });
  }

  onSearchClicked(searchCriteria: string){
    this.searchClicked.emit(searchCriteria);
  }
}
