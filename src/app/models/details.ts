export class Details {
  label: string;
  descr: string;

  constructor(label: string, descr:string) {
    this.label = label;
    this.descr = descr;
  }
}
