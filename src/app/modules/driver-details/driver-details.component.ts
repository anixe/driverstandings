import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { RaceTable } from './models/race-table';
import { OrderBy } from '../../models/ordery-by';
import { Race } from './models/race';
import { TableSortService } from '../../services/table-sort.service';
import { CommunicationService } from '../../services/communication.service';
import { TransformationService } from '../services/transformation.service';
import { Result } from './models/result';
import { race } from '../../../../node_modules/rxjs';
import { Constructor } from '../driver-standings/models/constructor';

@Component({
  selector: 'ds-driver-details',
  templateUrl: './driver-details.component.html',
  styleUrls: ['./driver-details.component.scss']
})
export class DriverDetailsComponent implements OnInit, OnDestroy {
  private id: string;
  public races: Race[];
  private orderBy: OrderBy = { isAsc: true, propertyPath: '' };

  constructor(private dataService: DataService,
    private communicationService: CommunicationService,
    private transformationService: TransformationService,
    private tableSortService: TableSortService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params['id'];
  }

  ngOnInit() {
    if (this.id) {
      this.dataService.getDriverDetails(this.id).subscribe(
        races => {
          this.races = races;
          this.setSidebarInfo(this.races[0].Results);
        },
        error => {
          alert('Cannot retrieve data');
        }
      );
    }
  }


  public sortBy(key: string) {
    if (this.orderBy.propertyPath === key) {
      this.orderBy.isAsc = !this.orderBy.isAsc;
    } else {
      this.orderBy = { isAsc: true, propertyPath: key };
    }

    this.races.sort(this.tableSortService.compareValues(this.orderBy));
  }

  private setSidebarInfo(raceResults: Result[]){
    if(raceResults){
      this.transformationService.resetSiderbarInfo();
      this.transformationService.addDriverToSidebarInfo(raceResults[0].Driver, raceResults[0].Constructor.name);
      this.communicationService.displayInfo(this.transformationService.sidebarInfoArray);
    }
  }

  ngOnDestroy(): void {
    this.transformationService.resetSiderbarInfo();
  }

}
