import { Header } from "./header";
import { Details } from "./details";

export interface SidebarInfo {
  isDriver: boolean;
  header?: Header;
  details: Details[];
  url: string;
}
