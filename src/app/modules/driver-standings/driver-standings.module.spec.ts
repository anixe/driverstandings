import { DriverStandingsModule } from './driver-standings.module';

describe('DriverStandingsModule', () => {
  let driverStandingsModule: DriverStandingsModule;

  beforeEach(() => {
    driverStandingsModule = new DriverStandingsModule();
  });

  it('should create an instance', () => {
    expect(driverStandingsModule).toBeTruthy();
  });
});
