import { Component, OnInit, Input } from '@angular/core';
import { Header } from '../../../../models/header';

@Component({
  selector: 'ds-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() header: Header;

  constructor() { }

  ngOnInit() {
  }

}
