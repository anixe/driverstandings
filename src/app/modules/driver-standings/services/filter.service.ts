import { Injectable } from '@angular/core';
import { DriverStanding } from '../models/driver-standing';
import { Constructor } from '../models/constructor';
import { Driver } from '../models/driver';

@Injectable()
export class FilterService {

  constructor() { }

  public filterBy(driverStandings: DriverStanding[], criteria: string):DriverStanding[]{
    if (!criteria) {
      return driverStandings;
    } else {
      criteria = criteria.toLowerCase();

      return driverStandings.filter((driverStanding) =>
      this.constructorNameContains(driverStanding.Constructors, criteria)
      || this.driverNameContains(driverStanding.Driver, criteria)
      || driverStanding.wins.includes(criteria));
    }
  }

  private constructorNameContains(constructors: Constructor[], criteria: string): boolean{
    if(constructors && constructors.length > 0){
      return constructors[0].name.toLowerCase().includes(criteria);
    }

    return false;
  }

  private driverNameContains(driver: Driver, criteria: string): boolean{
    return driver.familyName.toLowerCase().includes(criteria) || driver.givenName.toLowerCase().includes(criteria);
  }
}
