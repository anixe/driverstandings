import { StandingsList } from "./standings-list";

export class StandingsTable {
  season: string;
  StandingsLists: StandingsList[];
}
