import { Time2 } from "./time2";
import { AverageSpeed } from "./average-speed";

export interface FastestLap {
  rank: string;
  lap: string;
  Time: Time2;
  AverageSpeed: AverageSpeed;
}
