import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverDetailsComponent } from './driver-details.component';
import { Routes, RouterModule } from '@angular/router';
import { TransformationService } from '../services/transformation.service';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
  {path: 'driver-standings/:id' , component: DriverDetailsComponent}
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DriverDetailsComponent],
  providers: [TransformationService]
})
export class DriverDetailsModule { }
