import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverStandingsComponent } from './driver-standings.component';
import { Routes, RouterModule } from '@angular/router';
import { TransformationService } from '../services/transformation.service';
import { SharedModule } from '../../shared/shared.module';
import { FilterService } from './services/filter.service';

const routes: Routes = [
  {path: 'driver-standings' , component: DriverStandingsComponent}
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DriverStandingsComponent],
  providers: [TransformationService, FilterService]
})
export class DriverStandingsModule { }
