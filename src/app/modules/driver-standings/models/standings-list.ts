import { DriverStanding } from "./driver-standing";

export class StandingsList {
  season: string;
  round: string;
  DriverStandings: DriverStanding[];
}
