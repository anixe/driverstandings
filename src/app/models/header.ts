export interface Header {
  image?: string;
  nationalityImage?: string;
  name: string;
}
