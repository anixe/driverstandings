import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SidebarInfo } from '../models/sidebar-info';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  public sidebarInfo = new Subject<SidebarInfo[]>();

  constructor() { }

  public displayInfo(info: SidebarInfo[]){
    this.sidebarInfo.next(info);
  }
}
