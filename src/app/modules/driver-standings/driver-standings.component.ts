import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DriverStanding } from './models/driver-standing';
import { DataService } from '../services/data.service';
import { Driver } from './models/driver';
import { Constructor } from './models/constructor';
import { CommunicationService } from '../../services/communication.service';
import { SidebarInfo } from '../../models/sidebar-info';
import { Header } from '../../models/header';
import { TransformationService } from '../services/transformation.service';
import { FilterService } from './services/filter.service';
import { TableSortService } from '../../services/table-sort.service';
import { OrderBy } from '../../models/ordery-by';

@Component({
  selector: 'ds-driver-standings',
  templateUrl: './driver-standings.component.html',
  styleUrls: ['./driver-standings.component.scss']
})
export class DriverStandingsComponent implements OnInit, OnDestroy {
  @Output() driverClicked = new EventEmitter<Driver>();

  public driverStandingsOriginal: DriverStanding[]
  public driverStandings: DriverStanding[];
  private orderBy: OrderBy = { isAsc: true, propertyPath: '' };

  constructor(private dataService: DataService,
    private communicationService: CommunicationService,
    private transformationService: TransformationService,
    private filterService: FilterService, private tableSortService: TableSortService) {
  }

  ngOnInit() {
    this.dataService.getDriverStandings().subscribe((driverStandings) => {
      this.driverStandings = driverStandings;
      this.driverStandingsOriginal = driverStandings;
    }, (error) => {
      alert('Cannot retrieve standing list');
    });
  }

  public getDriverName(driver: Driver): string{
    return this.transformationService.getDriverName(driver);
  }

  public getContructorName(constructors: Constructor[]): string{
    return this.transformationService.getContructorName(constructors);
  }

  public onDriverNameClicked(driverStanding: DriverStanding){
    this.transformationService.resetSiderbarInfo();
    this.transformationService.addDriverToSidebarInfo(driverStanding.Driver, this.getContructorName(driverStanding.Constructors));
    this.transformationService.addContructorToSidebarInfo(driverStanding.Constructors);
    this.communicationService.displayInfo(this.transformationService.sidebarInfoArray);
  }

  public onSearchClicked(criteria) {
    this.driverStandings = this.filterService.filterBy(this.driverStandingsOriginal, criteria);
    this.sortRefreshedList();
  }

  public sortBy(key: string) {
    if (this.orderBy.propertyPath === key) {
      this.orderBy.isAsc = !this.orderBy.isAsc;
    } else {
      this.orderBy = { isAsc: true, propertyPath: key };
    }

    this.driverStandings.sort(this.tableSortService.compareValues(this.orderBy));
  }

  private sortRefreshedList(){
    this.orderBy.isAsc = !this.orderBy.isAsc;
    this.sortBy(this.orderBy.propertyPath);
  }

  ngOnDestroy(): void {
    this.transformationService.resetSiderbarInfo();
  }

}
