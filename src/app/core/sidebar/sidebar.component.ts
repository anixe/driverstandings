import { Component, OnInit, OnDestroy  } from '@angular/core';
import { CommunicationService } from '../../services/communication.service';
import { SidebarInfo } from '../../models/sidebar-info';

@Component({
  selector: 'ds-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  public siderbarInfoArray: SidebarInfo[];

  constructor(private communicationService: CommunicationService) {

  }

  ngOnInit() {
    this.communicationService.sidebarInfo.subscribe((info)=>{
      this.siderbarInfoArray = info;
    });
  }

  ngOnDestroy(): void {
    this.communicationService.sidebarInfo.unsubscribe();
  }

}
