import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { DriverStandingsModule } from './modules/driver-standings/driver-standings.module';
import { DriverDetailsModule } from './modules/driver-details/driver-details.module';

const routes: Routes = [
  {path: '' , redirectTo: 'driver-standings', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    DriverStandingsModule,
    DriverDetailsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
