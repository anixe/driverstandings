import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { BackgroundColorDirective } from './directives/background-color.directive';
import {ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports:[SearchComponent, BackgroundColorDirective],
  declarations: [SearchComponent, BackgroundColorDirective]
})
export class SharedModule { }
