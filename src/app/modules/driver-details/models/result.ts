import { Driver } from "../../driver-standings/models/driver";
import { Constructor } from "../../driver-standings/models/constructor";
import { FastestLap } from "./fastest-lap";
import { Time } from "./time";

export class Result {
  number: string;
  position: string;
  positionText: string;
  points: string;
  Driver: Driver;
  Constructor: Constructor;
  grid: string;
  laps: string;
  status: string;
  Time: Time;
  FastestLap: FastestLap;
}


