import { Component, OnInit, Input } from '@angular/core';
import { SidebarInfo } from '../../../models/sidebar-info';

@Component({
  selector: 'ds-sidebar-block',
  templateUrl: './sidebar-block.component.html',
  styleUrls: ['./sidebar-block.component.scss']
})
export class SidebarBlockComponent implements OnInit {
  @Input() sidebarInfo: SidebarInfo;

  constructor() { }

  ngOnInit() {
  }

}
