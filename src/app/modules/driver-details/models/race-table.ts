import { Race } from "./race";

export class RaceTable {
  season: string;
  driverId: string;
  Races: Race[];
}
