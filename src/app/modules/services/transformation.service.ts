import { Injectable } from "@angular/core";
import { SidebarInfo } from "../../models/sidebar-info";
import { Driver } from "../driver-standings/models/driver";
import { Header } from "../../models/header";
import { Constructor } from "../driver-standings/models/constructor";
import { Details } from "../../models/details";

@Injectable()
export class TransformationService {
  private readonly driverImage = 'https://d2d0b2rxqzh1q5.cloudfront.net/sv/2.183/dir/0fc/image/0fc2befadbf22802b7fdd49d3dc244a9.jpg';
  private readonly driverNationalityImage = 'https://flaglane.com/download/german-flag/german-flag-graphic.png';
  private readonly constructorImage = 'https://st-listas.20minutos.es/images/2013-05/360457/list_640px.jpg?1368624458';
  private readonly constructoNationalityImage = 'https://upload.wikimedia.org/wikipedia/commons/6/62/Flag_of_France.png';


  private _sidebarInfoArray: SidebarInfo[];

  constructor() {}

  public get sidebarInfoArray(): SidebarInfo[] {
    return this._sidebarInfoArray;
  }

  public resetSiderbarInfo() {
    this._sidebarInfoArray = new Array<SidebarInfo>();
  }

  public addDriverToSidebarInfo(driver: Driver, constructorName: string) {
    const sidebarInfo: SidebarInfo = <SidebarInfo>{
      isDriver: true,
      header: this.getHeader(this.getDriverName(driver), this.driverImage, this.driverNationalityImage),
      details: this.getDetailsFromDriver(driver, constructorName),
      url: driver.url
    };

    this._sidebarInfoArray.push(sidebarInfo);
  }

  public addContructorToSidebarInfo(constructors: Constructor[]) {
    const constructor = this.getConstructor(constructors);

    const sidebarInfo: SidebarInfo = <SidebarInfo>{
      isDriver: false,
      header: this.getHeader(this.getContructorName(constructors), this.constructorImage, this.constructoNationalityImage),
      details: new Array<Details>(),
      url: this.getConstructor(constructors).url
    };

    this._sidebarInfoArray.push(sidebarInfo);
  }

  public getDriverName(driver: Driver): string {
    return driver ? driver.givenName + " " + driver.familyName : "";
  }

  public getContructorName(contructors: Constructor[]): string {
    if (!contructors || contructors.length === 0) {
      return "";
    } else {
      return contructors[0].name;
    }
  }

  private getHeader(name: string, image?: string, nationalityImage?: string) {
    return <Header>{
      name: name,
      image: image,
      nationalityImage: nationalityImage
    };
  }

  private getDetailsFromDriver(driver: Driver, constructorName: string): Details[] {
    const details = new Array<Details>();

    details.push(new Details("Country", driver.nationality));
    details.push(new Details("Team", constructorName));
    details.push(new Details("Birth", driver.dateOfBirth));

    return details;
  }

  private getConstructor(constructors: Constructor[]): Constructor {
    if (constructors && constructors.length > 0) {
      return constructors[0];
    } else {
      return new Constructor();
    }
  }
}
