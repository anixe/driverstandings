export class OrderBy {
  isAsc: boolean;
  propertyPath: string;
}
