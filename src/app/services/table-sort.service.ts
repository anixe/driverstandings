import { Injectable } from '@angular/core';
import { OrderBy } from '../models/ordery-by';

@Injectable({
  providedIn: 'root'
})
export class TableSortService {

  constructor() { }

  public compareValues(orderBy: OrderBy) {
    const propPath = orderBy.propertyPath;
    const isAsc = orderBy.isAsc;

    return (a, b) => {
      const aProp = this.getPropertyFromPath(a, propPath.split(','));
      const bProp = this.getPropertyFromPath(b, propPath.split(','));

      if (!aProp || !bProp) {
        // property doesn't exist on either object
        return 0;
      }

      const varA = this.isInt(aProp) ? parseInt(aProp, 10) : (typeof aProp === 'string' ? aProp.toUpperCase() : aProp);
      const varB = this.isInt(bProp) ? parseInt(bProp, 10) : (typeof bProp === 'string' ? bProp.toUpperCase() : bProp);

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return isAsc === false ? comparison * -1 : comparison;
    };
  }


  private getPropertyFromPath(obj: Object, propPath: string[]): any{
    const key = propPath.shift();

    if (!key) {
      return undefined;
    } else {
      if (obj && obj.hasOwnProperty(key)) {
        if (propPath.length === 0) {
          return obj[key];
        } else {
          if (Array.isArray(obj[key])) {
            return this.getPropertyFromPath(obj[key][0], propPath);
          } else {
            return this.getPropertyFromPath(obj[key], propPath);
          }
        }
      }
    }
  }

  private isInt(n) {
    return /^[+-]?\d+$/.test(n);
  }
}
