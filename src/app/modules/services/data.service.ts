import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, OperatorFunction } from 'rxjs';
import { environment } from '../../../environments/environment.prod';
import { map} from 'rxjs/operators';
import { DriverStanding } from '../driver-standings/models/driver-standing';
import { RaceTable } from '../driver-details/models/race-table';
import { RootObject as dsRootObject} from '../driver-standings/models/root-object';
import { RootObject as ddRootObject} from '../driver-details/models/root-object';
import { Race } from '../driver-details/models/race';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private httpClient: HttpClient) { }

  getDriverStandings(): Observable<DriverStanding[]> {
    return this.httpClient.get<DriverStanding[]>(environment.endpoint + 'driverStandings.json').pipe(
      map((response: dsRootObject) => {
        if(response){
          return response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
        }
        return new Array<DriverStanding>();
      })
    );
  }

  getDriverDetails(driver: string): Observable<Race[]>{
    return this.httpClient.get<Race[]>(environment.endpoint + 'drivers/' + driver + '/results.json').pipe(
      map((response: ddRootObject) => {
        if(response){
          return response.MRData.RaceTable.Races;
        }
        return new Array<Race>();
      })
    );
  }
}
