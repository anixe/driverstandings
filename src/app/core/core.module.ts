import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarBlockComponent } from './sidebar/sidebar-block/sidebar-block.component';
import { HeaderComponent } from './sidebar/sidebar-block/header/header.component';
import { DetailsComponent } from './sidebar/sidebar-block/details/details.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [SidebarComponent, SidebarBlockComponent, HeaderComponent, DetailsComponent],
  exports: [SidebarComponent]
})
export class CoreModule { }
